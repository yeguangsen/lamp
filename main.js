import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false

Vue.prototype.$baseUrl = 'https://www.bbtongxue.top/';
Vue.prototype.$token = "";
Vue.prototype.$lamps = {
			    "msg":"",
			    "code":0,
			    "data":{
			        "total":12,
			        "list":[
			            {
			                "id":12,
			                "number":"e35660bbd27c424bb213249f15172087",
			                "code":"100001",
			                "name":"路灯01",
			                "status":2,
			                "lastHeartBeatTime":"2020-05-13 10:46:52",
			                "resetTime":2,
			                "area":null,
			                "createTime":"2020-01-05 11:25:36",
			                "updateTime":"2020-05-13 11:05:00",
							latitude:22.618813565184746,
							longitude:113.05718099361725
			            },
			            {
			                "id":13,
			                "number":"7c24bea1a82f478d9d35991c2126a23f",
			                "code":"100002",
			                "name":"路灯02",
			                "status":2,
			                "lastHeartBeatTime":"2020-05-13 10:46:58",
			                "resetTime":10,
			                "area":null,
			                "createTime":"2020-01-05 11:25:59",
			                "updateTime":"2020-05-13 11:00:00",
							latitude:22.618813565184746,
							longitude:113.05638099361725
			            },
			            {
			                "id":14,
			                "number":"602e87e8638346d290c28685f5e94232",
			                "code":"100003",
			                "name":"路灯03",
			                "status":2,
			                "lastHeartBeatTime":"2020-05-13 10:46:36",
			                "resetTime":1,
			                "area":null,
			                "createTime":"2020-01-09 11:02:16",
			                "updateTime":"2020-05-13 11:00:00",
							latitude:22.618813565184746,
							longitude:113.45638099361725
			            },
			            {
			                "id":15,
			                "number":"baec4407d7164c1d9f5e32fe02bfa11e",
			                "code":"100004",
			                "name":"路灯04",
			                "status":2,
			                "lastHeartBeatTime":"2020-05-13 10:46:41",
			                "resetTime":0,
			                "area":null,
			                "createTime":"2020-01-09 11:02:55",
			                "updateTime":"2020-05-13 11:00:00",
							latitude:22.518813565184746,
							longitude:113.45638099361725
			            },
			            {
			                "id":16,
			                "number":"4098bb42fe30468ba2a3b8fb7cdfd9a6",
			                "code":"100005",
			                "name":"路灯05",
			                "status":2,
			                "lastHeartBeatTime":"2020-05-13 10:46:47",
			                "resetTime":1,
			                "area":null,
			                "createTime":"2020-01-09 11:04:37",
			                "updateTime":"2020-05-13 11:00:00",
							latitude:22.718813565184746,
							longitude:113.65638099361725
			            },
			            {
			                "id":17,
			                "number":"94ba08f30682478e850bd80dfab21f0b",
			                "code":"100006",
			                "name":"路灯06",
			                "status":2,
			                "lastHeartBeatTime":"2020-05-13 10:46:46",
			                "resetTime":1,
			                "area":null,
			                "createTime":"2020-01-09 11:04:48",
			                "updateTime":"2020-05-13 11:00:00",
							latitude:22.718813565184746,
							longitude:113.85638099361725
			            },
			            {
			                "id":18,
			                "number":"8b71dd7d67d8464895c2646de19fc8b3",
			                "code":"100007",
			                "name":"路灯07",
			                "status":2,
			                "lastHeartBeatTime":"2020-05-13 10:46:52",
			                "resetTime":1,
			                "area":null,
			                "createTime":"2020-01-09 11:04:57",
			                "updateTime":"2020-05-13 11:00:00",
							latitude:22.718813565184746,
							longitude:113.85638099361725
			            },
			            {
			                "id":19,
			                "number":"3cda091bb2d34f789b17b92439b8e38d",
			                "code":"100008",
			                "name":"路灯08",
			                "status":2,
			                "lastHeartBeatTime":"2020-05-13 10:46:57",
			                "resetTime":1,
			                "area":null,
			                "createTime":"2020-01-09 11:05:08",
			                "updateTime":"2020-05-13 11:00:00",
							latitude:22.738813565184746,
							longitude:113.65638099361725
			            },
			            {
			                "id":20,
			                "number":"f0e6a737dee841719300f33e28677898",
			                "code":"100009",
			                "name":"路灯09",
			                "status":2,
			                "lastHeartBeatTime":"2020-05-13 10:46:35",
			                "resetTime":1,
			                "area":null,
			                "createTime":"2020-01-09 11:05:17",
			                "updateTime":"2020-05-13 11:00:00",
							latitude:22.718813565184746,
							longitude:113.67638099361725
			            },
			            {
			                "id":21,
			                "number":"254bf6c3c17a4be9af9dad709bfa6458",
			                "code":"100010",
			                "name":"路灯10",
			                "status":2,
			                "lastHeartBeatTime":"2020-05-13 10:46:41",
			                "resetTime":1,
			                "area":null,
			                "createTime":"2020-01-09 11:05:31",
			                "updateTime":"2020-05-13 11:00:00",
							latitude:22.713813565184746,
							longitude:113.65638099361725
			            },
			            {
			                "id":22,
			                "number":"5f8ff87d01084103a9662bba8afb8e3d",
			                "code":"000011",
			                "name":"路灯11",
			                "status":2,
			                "lastHeartBeatTime":"2020-04-28 20:57:07",
			                "resetTime":1,
			                "area":null,
			                "createTime":"2020-04-28 20:57:07",
			                "updateTime":"2020-05-13 11:05:00",
							latitude:22.718113565184746,
							longitude:113.65638099361725
			            },
			            {
			                "id":23,
			                "number":"0939fe1a45194d0bbe8fe46038c16e92",
			                "code":"100012",
			                "name":"路灯12",
			                "status":2,
			                "lastHeartBeatTime":"2020-05-13 11:01:45",
			                "resetTime":1,
			                "area":null,
			                "createTime":"2020-05-13 11:01:45",
			                "updateTime":"2020-05-13 11:15:00",
							latitude:22.718813565184746,
							longitude:113.23638099361725
			            }
			        ],
			        "pageNum":1,
			        "pageSize":20,
			        "size":12,
			        "startRow":1,
			        "endRow":12,
			        "pages":1,
			        "prePage":0,
			        "nextPage":0,
			        "isFirstPage":true,
			        "isLastPage":true,
			        "hasPreviousPage":false,
			        "hasNextPage":false,
			        "navigatePages":8,
			        "navigatepageNums":[
			            1
			        ],
			        "navigateFirstPage":1,
			        "navigateLastPage":1
			    }
			}

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()